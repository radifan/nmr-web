<?php

namespace App\Models\Invoice;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetails extends Model
{
    //

    protected $table = 'tb_invoicedetail';

    protected $fillable = ['detail_no', 'detail_date', 'detail_desc', 'detail_header', 'detail_debit', 'detail_credit', 'invoice_id'];

    public function invoice() {
        return $this->belongsTo('App\Models\Invoice\Invoice', 'invoice_id');
    }
}
