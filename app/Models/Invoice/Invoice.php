<?php

namespace App\Models\Invoice;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    //

    protected $table = 'tb_invoice';

    protected $fillable = ['date', 'type', 'branch'];

    public function branches() {
        return $this->belongsTo('App\Models\Core\Branch', 'branch');
    }

    public function details() {
        return $this->hasMany('App\Models\Invoice\InvoiceDetails', 'invoice_id');
    }
}
