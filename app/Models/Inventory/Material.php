<?php

namespace App\Models\Inventory;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    //

    protected $table = 'tb_material';

    protected $fillable = ['quantity', 'min_quantity', 'quantity_type', 'material_desc_id', 'material_desc_en', 'price', 'branch'];

    public function branches() {
        return $this->belongsTo('App\Models\Core\Branch', 'branch');
    }
}
