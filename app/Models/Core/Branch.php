<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    //

    protected $table = 'tb_branch';

    protected $fillable = ['branch_name', 'branch_id', 'address'];
}
