<?php

namespace App\Models\PurchaseOrder;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    //

    protected $table = 'tb_purchaseorder';

    protected $fillable = ['date', 'code', 'region', 'name', 'status'];

    public function details() {
        return $this->hasMany('App\Models\PurchaseOrder\PurchaseOrderDetails', 'po_parent_id');
    }
}
