<?php

namespace App\Models\PurchaseOrder;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderDetails extends Model
{
    //

    protected $table = 'tb_purchaseorderdetail';

    protected $fillable = ['po_code', 'po_name', 'po_quantity', 'po_type', 'po_branch', 'po_parent_id'];

    public function branch() {
        return $this->belongsTo('App\Models\Core\Branch', 'branch');
    }
}
