<?php

namespace App\Http\Controllers\Backend\Inventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Backend\Inventory\MaterialRepository;
use App\Repositories\Backend\Branch\BranchRepository;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserController.
 */
class InventoryController extends Controller
{

    protected $material, $branches;

    public function __construct(MaterialRepository $material, BranchRepository $branches)
    {
        $this->material = $material;
        $this->branches = $branches;
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();
        $role = $user->roles[0]->id;
        $branch = $user->branch;
        if ($role == 1) {
            $materials = $this->material->getAllPaginate(15);
        } else {
            $materials = $this->material->getPaginationByBranch($branch);
        }
        return view('backend.inventory.index')->with('materials', $materials);
    }

    public function create()
    {
        $user = Auth::user();
        $branch = $user->branches;
        $role = $user->roles[0]->id;
        $isAdmin = false;
        if ($role == 1) {
            $branch = $this->branches->getAll();
            $isAdmin = true;
        }
        return view('backend.inventory.create')->with(['branch' => $branch, 'isAdmin' => $isAdmin]);
    }

    public function store(Request $request)
    {
        $this->material->create($request->all());

        return redirect()->route('admin.inventory.index')->withFlashSuccess('Material baru telah berhasil ditambahkan');
    }

    public function edit($id)
    {
        $materials = $this->material->find($id);
        $user = Auth::user();
        $branch = [];
        $role = $user->roles[0]->id;
        if ($role == 1) {
            $branch = $this->branches->getAll();
        }
        return view('backend.inventory.edit')->with(['material' => $materials, 'branch' => $branch]);
    }

    public function update($id, Request $request)
    {
        $this->material->update($id, $request->all());

        return redirect()->route('admin.inventory.index')->withFlashSuccess('Data material telah berhasil diubah');
    }

    public function destroy($id)
    {
        $this->material->destroy($id);

        return redirect()->route('admin.inventory.index')->withFlashSuccess('Data material telah berhasil dihapus');
    }

    public function getItem($id)
    {
        $materials = $this->material->find($id);
        return view('backend.inventory.get-item')->with('material', $materials);
    }

    public function getItemPost($id, Request $request)
    {
        $user = Auth::user();
        $this->material->getItem($id, $request->all(), $user);
        return redirect()->route('admin.inventory.index')->withFlashSuccess('Item telah berhasil diambil');
    }
}
