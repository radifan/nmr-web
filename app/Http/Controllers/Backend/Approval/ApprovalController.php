<?php

namespace App\Http\Controllers\Backend\Approval;

use App\Http\Controllers\Controller;
use App\Models\PurchaseOrder\PurchaseOrder;
use Illuminate\Http\Request;
use App\Repositories\Backend\PurchaseOrder\PurchaseOrderRepository;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserController.
 */
class ApprovalController extends Controller
{

    protected $po;

    public function __construct(PurchaseOrderRepository $po)
    {
        $this->po = $po;
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $pendingPo = $this->po->getAllPending();
        return view('backend.approval.index')->with('pos', $pendingPo);
    }

    public function show($id)
    {
        $po = $this->po->find($id);
        return view('backend.approval.show')->with('po', $po);
    }

    public function create()
    {
        $branch = Auth::user()->branches;
        return view('backend.inventory.create')->with('branch', $branch);
    }

    public function store(Request $request)
    {
        $this->material->create($request->all());

        return redirect()->route('admin.inventory.index')->withFlashSuccess('Material baru telah berhasil ditambahkan');
    }

    public function edit($id)
    {
        $materials = $this->material->find($id);
        return view('backend.inventory.edit')->with('material', $materials);
    }

    public function approve($id)
    {
        $this->po->approve($id);

        return redirect()->route('admin.approval.index')->withFlashSuccess('Purchase order telah berhasil di-approve');
    }

    public function destroy($id)
    {
        $this->material->destroy($id);

        return redirect()->route('admin.inventory.index')->withFlashSuccess('Data material telah berhasil dihapus');
    }
}
