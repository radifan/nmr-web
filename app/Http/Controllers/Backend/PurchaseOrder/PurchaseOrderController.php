<?php

namespace App\Http\Controllers\Backend\PurchaseOrder;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Backend\PurchaseOrder\PurchaseOrderRepository;
use App\Repositories\Backend\Branch\BranchRepository;
use App\Repositories\Backend\Inventory\MaterialRepository;

/**
 * Class UserController.
 */
class PurchaseOrderController extends Controller
{

    protected $po, $branch, $material;

    public function __construct(PurchaseOrderRepository $po, BranchRepository $branch, MaterialRepository $material)
    {
        $this->po = $po;
        $this->branch = $branch;
        $this->material = $material;
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $pos = $this->po->getAllPaginate(15);
        return view('backend.po.index')->with('pos', $pos);
    }

    public function show($id)
    {
        $pos = $this->po->find($id);
        $branches = $this->branch->getAll();
        return view('backend.po.show')->with(compact(['pos', 'branches']));
    }

    public function create()
    {
        $branches = $this->branch->getAll();
        $posId = $this->po->setId();
        $materials = $this->material->getAll();

        $inventoryElement = '';
        foreach ($materials as $material) {
            $inventoryElement = $inventoryElement . "<option value=" . $material->id .">" . $material->sku . " - " . $material->material_desc_id . "</option>";
        }

        return view('backend.po.create')->with(['branches' => $branches, 'posId' => $posId, 'inventory' => $inventoryElement]);
    }

    public function store(Request $request)
    {
        $this->po->create($request->all());

        return redirect()->route('admin.po.index')->withFlashSuccess('Purchase order baru telah berhasil ditambahkan');
    }

    public function edit($id)
    {
        $pos = $this->po->find($id);
        $branches = $this->branch->getAll();
        return view('backend.po.edit')->with(compact(['pos', 'branches']));
    }

    public function update($id, Request $request)
    {
        $this->po->update($id, $request->all());

        return redirect()->route('admin.po.index')->withFlashSuccess('Purchase order telah berhasil diubah');
    }

    public function destroy($id)
    {
        $this->po->destroy($id);

        return redirect()->route('admin.po.index')->withFlashSuccess('Purchase order telah berhasil dihapus');
    }

    public function printPO($id)
    {
        $pos = $this->po->find($id);
        return view('backend.po.print')->with('pos', $pos);
    }
}
