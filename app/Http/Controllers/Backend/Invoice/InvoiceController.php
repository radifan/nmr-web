<?php

namespace App\Http\Controllers\Backend\Invoice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Backend\Invoice\InvoiceRepository;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserController.
 */
class InvoiceController extends Controller
{

    protected $invoice;

    public function __construct(InvoiceRepository $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $invoices = $this->invoice->getAllPaginate(15);
        return view('backend.invoice.index')->with('invoices', $invoices);
    }

    public function create()
    {
        $branch = Auth::user()->branches;
        return view('backend.invoice.create')->with('branch', $branch);
    }

    public function store(Request $request)
    {
        $this->invoice->create($request->all());

        return redirect()->route('admin.invoice.index')->withFlashSuccess('Invoice baru telah berhasil ditambahkan');
    }

    public function show($id)
    {
        $invoices = $this->invoice->find($id);
        return view('backend.invoice.show')->with('invoices', $invoices);
    }

    public function edit($id)
    {
        $invoices = $this->invoice->find($id);
        return view('backend.invoice.edit')->with('invoices', $invoices);
    }

    public function update($id, Request $request)
    {
        $this->invoice->update($id, $request->all());

        return redirect()->route('admin.invoice.index')->withFlashSuccess('Invoice telah berhasil diubah');
    }

    public function destroy($id)
    {
        $this->invoice->destroy($id);

        return redirect()->route('admin.invoice.index')->withFlashSuccess('Invoice telah berhasil dihapus');
    }
}
