<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Inventory\MaterialRepository;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    protected $material;

    public function __construct(MaterialRepository $material)
    {
        $this->material = $material;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $materials = $this->material->getLowStockMaterials();
        return view('backend.dashboard')->with('materials', $materials);
    }
}
