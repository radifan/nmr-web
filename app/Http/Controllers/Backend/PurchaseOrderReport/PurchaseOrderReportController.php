<?php

namespace App\Http\Controllers\Backend\PurchaseOrderReport;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Backend\PurchaseOrder\PurchaseOrderRepository;
use App\Repositories\Backend\Branch\BranchRepository;

/**
 * Class UserController.
 */
class PurchaseOrderReportController extends Controller
{

    protected $po, $branch;

    public function __construct(PurchaseOrderRepository $po, BranchRepository $branch)
    {
        $this->po = $po;
        $this->branch = $branch;
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $pos = $this->po->getAllPaginate(15);
        return view('backend.po-report.index')->with('pos', $pos);
    }

    public function show($id)
    {
        $pos = $this->po->find($id);
        $branches = $this->branch->getAll();
        return view('backend.po-report.show')->with(compact(['pos', 'branches']));
    }

    public function create()
    {
        $branches = $this->branch->getAll();
        return view('backend.po-report.create')->with('branches', $branches);
    }

    public function store(Request $request)
    {
        $this->po->create($request->all());

        return redirect()->route('admin.po-report.index')->withFlashSuccess('Purchase order baru telah berhasil ditambahkan');
    }

    public function edit($id)
    {
        $pos = $this->po->find($id);
        $branches = $this->branch->getAll();
        return view('backend.po-report.edit')->with(compact(['pos', 'branches']));
    }

    public function update($id, Request $request)
    {
        $this->po->updateReport($id, $request->all());

        return redirect()->route('admin.po-report.index')->withFlashSuccess('Laporan purchase order telah berhasil diubah');
    }

    public function destroy($id)
    {
        $this->po->destroy($id);

        return redirect()->route('admin.po-report.index')->withFlashSuccess('Purchase order telah berhasil dihapus');
    }
}
