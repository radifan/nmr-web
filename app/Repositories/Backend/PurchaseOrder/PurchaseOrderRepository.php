<?php

namespace App\Repositories\Backend\PurchaseOrder;

use App\Models\PurchaseOrder\PurchaseOrder;
use App\Models\Inventory\Material;
use Illuminate\Support\Facades\DB;
use App\Repositories\BaseRepository;

/**
 * Class RoleRepository.
 */
class PurchaseOrderRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = PurchaseOrder::class;

    /**
     * @param string $order_by
     * @param string $sort
     *
     * @return mixed
     */
    public function getById($id)
    {
        return $this->query()->find($id);
    }

    public function getAllPending()
    {
        return $this->query()->where('status', '=', 'PENDING')->paginate(15);
    }

    public function approve($id)
    {
        DB::transaction(function () use ($id) {
            $po = $this->query()->find($id);
            $po->status = 'APPROVED';
            $po->save();
        });
    }

    public function create(array $input)
    {
        DB::transaction(function () use ($input) {
            $po = self::MODEL;
            $po = new $po();
            $po->fill($input);
            $po->code = $this->setId();
            $po->status = 'PENDING';

            $po->save();

            for ($i = 0; $i < count($input['po_code']); $i++) {
                $material = Material::find($input['po_code'][$i]);

                $po->details()->create([
                    'po_code' => $material->sku,
                    'po_name' => $material->material_desc_id,
                    'po_quantity' => $input['po_quantity'][$i],
                    'po_type' => $input['po_type'][$i],
                    'po_branch' => $input['po_branch'][$i]
                ]);
            }
        });
    }

    public function update($id, array $input)
    {
        DB::transaction(function () use ($id, $input) {
            $po = $this->query()->find($id);
            $po->fill($input);

            $po->save();

            for ($i = 0; $i < count($input['po_code']); $i++) {
                $material = Material::find($input['po_code'][$i]);

                if (isset($input['id'][$i])) {
                    $po->details[$i]->po_code = $material->sku;
                    $po->details[$i]->po_name = $material->material_desc_id;
                    $po->details[$i]->po_quantity = $input['po_quantity'][$i];
                    $po->details[$i]->po_type = $input['po_type'][$i];
                    $po->details[$i]->po_branch = $input['po_branch'][$i];
                    $po->details[$i]->save();
                } else {
                    $po->details()->create([
                        'po_code' => $material->sku,
                        'po_name' => $material->material_desc_id,
                        'po_quantity' => $input['po_quantity'][$i],
                        'po_type' => $input['po_type'][$i],
                        'po_branch' => $input['po_branch'][$i]
                    ]);
                }
            }
        });
    }

    public function updateReport($id, array $input)
    {
        DB::transaction(function () use ($id, $input) {
            $po = $this->query()->find($id);
            $po->fill($input);

            $po->save();

            $total = 0;

            for ($i = 0; $i < count($input['po_code']); $i++) {
                if (isset($input['id'][$i])) {
                    $po->details[$i]->po_code = $input['po_code'][$i];
                    $po->details[$i]->po_name = $input['po_name'][$i];
                    $po->details[$i]->po_quantity = $input['po_quantity'][$i];
                    $po->details[$i]->po_price = $input['po_price'][$i];
                    $po->details[$i]->po_type = $input['po_type'][$i];
                    $po->details[$i]->po_branch = $input['po_branch'][$i];
                    $po->details[$i]->save();

                    $total = $total + $input['po_price'][$i];
                } else {
                    $po->details()->create([
                        'po_code' => $input['po_code'][$i],
                        'po_name' => $input['po_name'][$i],
                        'po_quantity' => $input['po_quantity'][$i],
                        'po_type' => $input['po_type'][$i],
                        'po_branch' => $input['po_branch'][$i]
                    ]);
                }
            }

            $po->total_price = $total;
            $po->save();
        });
    }

    public function destroy($id)
    {
        $invoice = $this->query()->find($id);
        $invoice->delete();
    }

    public function setId() {
        $lastId = ((int) DB::table('tb_purchaseorder')->max('id')) + 1;
        while (strlen((string) $lastId) <= 6) {
            $lastId = '0' . $lastId;
        }
        return 'PO' . $lastId;
    }
}
