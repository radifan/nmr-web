<?php

namespace App\Repositories\Backend\Inventory;

use App\Models\Inventory\Material;
use Illuminate\Support\Facades\DB;
use App\Repositories\BaseRepository;

/**
 * Class RoleRepository.
 */
class MaterialRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Material::class;

    /**
     * @param string $order_by
     * @param string $sort
     *
     * @return mixed
     */

    public function getPaginationByBranch($branch)
    {
        return $this->query()
            ->where('branch', $branch)
            ->latest()
            ->paginate(15);
    }

    public function getById($id)
    {
        return $this->query()->find($id);
    }

    public function create(array $input)
    {
        DB::transaction(function () use ($input) {
            $material = self::MODEL;
            $material = new $material();
            $material->fill($input);

            $material->save();

            $sku = $material->id;
            while (strlen((string) $sku) <= 6) {
                $sku = '0' . $sku;
            }

            $material->sku = $material->branches->branch_id . '-' . $sku;
            $material->save();
        });
    }

    public function update($id, array $input)
    {
        DB::transaction(function () use ($id, $input) {
            $material = $this->query()->find($id);
            $material->fill($input);

            $material->save();
        });
    }

    public function getItem($id, array $input, $user)
    {
        DB::transaction(function () use ($id, $input, $user) {
            $material = $this->query()->find($id);

            if ($input['quantity'] > $material->quantity) {
                $input['quantity'] = $material->quantity;
            }

            $material->quantity = $material->quantity - $input['quantity'];

            history()->withType('Inventory')
                ->withEntity($user->id)
                ->withText('get ' . $input['quantity'] . ' {item}')
                ->withIcon('minus')
                ->withClass('bg-yellow')
                ->withAssets([
                    'item_link' => ['admin.inventory.edit', $material->material_desc_id, $material->id],
                ])
                ->log();

            $material->save();
        });
    }

    public function destroy($id)
    {
        $material = $this->query()->find($id);
        $material->delete();
    }

    public function getLowStockMaterials()
    {
        return $this->query()
            ->whereColumn('quantity', '<', 'min_quantity')
            ->get();
    }

}
