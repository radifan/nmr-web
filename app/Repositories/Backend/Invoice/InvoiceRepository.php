<?php

namespace App\Repositories\Backend\Invoice;

use App\Models\Invoice\Invoice;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RoleRepository.
 */
class InvoiceRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Invoice::class;

    /**
     * @param string $order_by
     * @param string $sort
     *
     * @return mixed
     */
    public function getById($id)
    {
        return $this->query()->find($id);
    }

    public function create(array $input)
    {
        DB::transaction(function () use ($input) {
            $invoice = self::MODEL;
            $invoice = new $invoice();
            $invoice->fill($input);

            $invoice->save();

            for ($i = 0; $i < count($input['detail_desc']); $i++) {
                $invoice->details()->create([
                    'detail_no' => $i + 1,
                    'detail_date' => $input['detail_date'][$i],
                    'detail_desc' => $input['detail_desc'][$i],
                    'detail_header' => $input['detail_header'][$i],
                    'detail_debit' => $input['detail_debit'][$i],
                    'detail_credit' => $input['detail_credit'][$i]
                ]);
            }
        });
    }

    public function update($id, array $input)
    {
        DB::transaction(function () use ($id, $input) {
            $invoice = $this->query()->find($id);
            $invoice->fill($input);

            $invoice->save();

            for ($i = 0; $i < count($input['detail_desc']); $i++) {
                if (isset($input['id'][$i])) {
                    $invoice->details[$i]->detail_no = $i + 1;
                    $invoice->details[$i]->detail_date = $input['detail_date'][$i];
                    $invoice->details[$i]->detail_desc = $input['detail_desc'][$i];
                    $invoice->details[$i]->detail_header = $input['detail_header'][$i];
                    $invoice->details[$i]->detail_debit = $input['detail_debit'][$i];
                    $invoice->details[$i]->detail_credit = $input['detail_credit'][$i];
                    $invoice->details[$i]->save();
                } else {
                    $invoice->details()->create([
                        'detail_no' => $i + 1,
                        'detail_date' => $input['detail_date'][$i],
                        'detail_desc' => $input['detail_desc'][$i],
                        'detail_header' => $input['detail_header'][$i],
                        'detail_debit' => $input['detail_debit'][$i],
                        'detail_credit' => $input['detail_credit'][$i]
                    ]);
                }
            }
        });
    }

    public function destroy($id)
    {
        $invoice = $this->query()->find($id);
        $invoice->delete();
    }
}
