<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>Purchase Order</title>

    <script>
        window.onload = function() {
            window.print();
        }
    </script>
</head>
<body>
    <div style="padding-bottom: 20px; padding-top: 20px">
        Code: <b>{{$pos->code}}</b><br />
        Region: <b>{{$pos->region}}</b><br />
        To: <b>{{$pos->name}}</b><br />
    </div>
    <div style="padding-bottom: 10px">
        <table border="1" width="100%">
            <thead>
                <th colspan="2" width="10%">Qty</th>
                <th width="20%">Item Code</th>
                <th width="40%">Item Name</th>
                <th width="30%">Price</th>
            </thead>
            <tbody>
                @foreach ($pos->details as $po)
                    <tr>
                        <td>{{$po->po_quantity}}</td>
                        <td>{{$po->po_type}}</td>
                        <td>{{$po->po_code}}</td>
                        <td>{{$po->po_name}}</td>
                        <td style="text-align: right">{{$po->po_price ? "Rp." . number_format($po->po_price) : "Rp.0" }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="2" width="10%"></td>
                    <td width="20%"></td>
                    <td width="40%"><b>Total Price</b></td>
                    <td width="30%" style="text-align: right"><b>{{$pos->total_price ? "Rp." . number_format($pos->total_price) : "Rp.0"}}</b></td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>