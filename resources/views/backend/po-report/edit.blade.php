@extends ('backend.layouts.app')

@section ('title', 'Edit Purchase Order' . ' | ' . trans('labels.backend.access.roles.create'))

@section('page-header')
    <h1>
        Inventory Management
        <small>Edit Purchase Order</small>
    </h1>
@endsection

@section('content')
    {{ Form::open(['route' => ['admin.po-report.update', $pos->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'patch', 'id' => 'edit-po']) }}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Purchase Order</h3>
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Tanggal
                    </label>

                    <div class="col-lg-10">
                        <input type="date" name="date" class="form-control" value="{{date('Y-m-d', strtotime($pos->date))}}">
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Kode
                    </label>

                    <div class="col-lg-10">
                        <input type="text" name="code" class="form-control" value="{{$pos->code}}">
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Area
                    </label>

                    <div class="col-lg-10">
                        <input type="text" name="region" class="form-control" value="{{$pos->region}}">
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Nama
                    </label>

                    <div class="col-lg-10">
                        <input type="text" name="name" class="form-control" value="{{$pos->name}}">
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Detail
                    </label>

                    <div class="col-lg-10">
                        <div class="well well-lg">
                            @php $i = 0 @endphp
                            <div id="detail-container">
                                @foreach ($pos->details as $detail)
                                <div id="po-detail-0" class="form-group po-detail">
                                    <input type="hidden" name="id[]" value="{{$detail->id}}">
                                    <div class="col-lg-2">
                                        <input type="text" name="po_code[]" class="form-control" placeholder="Kode" value="{{$detail->po_code}}">
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="text" name="po_name[]" class="form-control" placeholder="Nama" value="{{$detail->po_name}}">
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="number" name="po_quantity[]" class="form-control" placeholder="Jumlah" value="{{$detail->po_quantity}}">
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="number" name="po_price[]" class="form-control price" placeholder="Harga" value="{{$detail->po_price}}">
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="text" name="po_type[]" class="form-control" placeholder="Tipe Barang" value="{{$detail->po_type}}">
                                    </div>
                                    <div class="col-lg-2">
                                        <select class="form-control" name="po_branch[]">
                                            @foreach($branches as $branch)
                                                @if ($detail->po_branch == $branch->id)
                                                    <option value="{{$branch->id}}" selected>{{$branch->branch_name}}</option>
                                                @else
                                                    <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @php $i = $i + 1 @endphp
                                @endforeach
                            </div>

                            <div class="pull-right">
                                <a href="#" onclick="addDetails()" class="btn btn-info btn-xs">Tambah Detail</a>
                            </div><!--pull-right-->

                            <br/>
                        </div>
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.po.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.edit'), ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@endsection

@section('after-scripts')
    <script>
        var detailCount = {{$i}};

        function addDetails() {
            var inner = '<div class="col-lg-2"> <input type="text" name="po_code[]" class="form-control" placeholder="Kode"> </div> <div class="col-lg-2"> <input type="text" name="po_name[]" class="form-control" placeholder="Nama"> </div> <div class="col-lg-2"> <input type="number" name="po_quantity[]" class="form-control" placeholder="Jumlah"> </div><div class="col-lg-2"> <input type="number" name="po_price[]" class="form-control price" placeholder="Harga"> </div> <div class="col-lg-2"> <input type="text" name="po_type[]" class="form-control" placeholder="Tipe Barang"> </div> <div class="col-lg-2"> <select class="form-control" name="po_branch[]"> @foreach($branches as $branch) <option value="{{$branch->id}}">{{$branch->branch_name}}</option> @endforeach </select> </div>';
            var newDetail = document.createElement('div');
            newDetail.id = 'po-detail-' + detailCount;
            newDetail.classList = 'form-group invoice-detail';
            newDetail.innerHTML = inner;

            document.getElementById('detail-container').appendChild(newDetail);
            detailCount += 1;
        }
    </script>
@endsection
