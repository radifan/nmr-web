@extends ('backend.layouts.app')

@section ('title', 'Purchase Order Report')

@section('after-styles')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
@endsection

@section('page-header')
    <h1>
        Purchase Order Report
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Purchase Order Report List</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Tanggal</th>
                            <th>Kode</th>
                            <th>Area</th>
                            <th>Nama</th>
                            <th>Total Harga</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pos as $po)
                        <tr>
                            <td>{{$po->id}}</td>
                            <td>{{date('m-d-Y', strtotime($po->date))}}</td>
                            <td>{{$po->code}}</td>
                            <td>{{$po->region}}</td>
                            <td>{{$po->name}}</td>
                            <td class="price">Rp.{{number_format($po->total_price)}}</td>
                            <td>{{$po->status}}</td>
                            <td>
                                <a href="{{url('/admin/po-report')}}/{{$po->id}}" class="btn btn-xs btn-info"><i class="fa fa-search" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"></i></a>
                                @if ($po->status == 'PENDING')
                                <a href="{{url('/admin/po-report')}}/{{$po->id}}/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i></a>
                                @endif
                                <a href="{{url('/admin/po-print')}}/{{$po->id}}" class="btn btn-xs btn-info"><i class="fa fa-print" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print"></i></a>
                                <a data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Delete" data-trans-title="Are you sure?" class="btn btn-xs btn-danger" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                                    <form action="{{url('/admin/po')}}/{{$po->id}}" method="POST" id="delete-item-{{$po->id}}" style="display:none">
                                        <input type="hidden" name="_method" value="delete">
                                        {{ csrf_field() }}
                                    </form>
                                </a>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!--table-responsive-->

            {{$pos->links()}}
        </div><!-- /.box-body -->
    </div><!--box-->

@endsection