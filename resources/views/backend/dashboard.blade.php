@extends('backend.layouts.app')

@section('page-header')
    <h1>
        {{ app_name() }}
        <small>{{ trans('strings.backend.dashboard.title') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Low Stock Materials</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Jumlah</th>
                        <th>Jumlah Minimum</th>
                        <th>Satuan</th>
                        <th>Nama</th>
                        <th>Nama (Bahasa Inggris)</th>
                        <th>Cabang</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($materials as $material)
                        <tr>
                            <td>{{$material->sku}}</td>
                            <td class="price">{{$material->quantity}}</td>
                            <td class="price">{{$material->min_quantity}}</td>
                            <td>{{$material->quantity_type}}</td>
                            <td>{{$material->material_desc_id}}</td>
                            <td>{{$material->material_desc_en}}</td>
                            <td>{{$material->branches->branch_name}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box box-success-->

    @permission('view-history')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {!! history()->render() !!}
        </div><!-- /.box-body -->
    </div><!--box box-success-->
    @endauth
@endsection