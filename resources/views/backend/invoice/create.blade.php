@extends ('backend.layouts.app')

@section ('title', 'Add Invoice' . ' | ' . trans('labels.backend.access.roles.create'))

@section('page-header')
    <h1>
        Inventory Management
        <small>Create Invoice</small>
    </h1>
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.invoice.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-invoice']) }}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Create Invoice</h3>
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Tanggal
                    </label>

                    <div class="col-lg-10">
                        <input type="date" name="date" class="form-control">
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Tipe
                    </label>

                    <div class="col-lg-10">
                        <input type="text" name="type" class="form-control" placeholder="Laporan Pengeluaran" >
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Cabang
                    </label>

                    <div class="col-lg-10">
                        <input type="hidden" name="branch" value="{{$branch->id}}">
                        <input type="text" name="branch_name" class="form-control" placeholder="Jeruk" value="{{$branch->branch_name}}" disabled>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Detail
                    </label>

                    <div class="col-lg-10">
                        <div class="well well-lg">
                            <div id="detail-container">
                                <div id="invoice-detail-0" class="form-group invoice-detail">
                                    <div class="col-lg-2">
                                        <input type="date" name="detail_date[]" class="form-control" placeholder="Tanggal">
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" name="detail_desc[]" class="form-control" placeholder="Deskripsi">
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" name="detail_header[]" class="form-control" placeholder="Detail Grup">
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="number" name="detail_debit[]" class="form-control" placeholder="Debit">
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="number" name="detail_credit[]" class="form-control" placeholder="Credit">
                                    </div>
                                </div>
                            </div>

                            <div class="pull-right">
                                <a href="#" onclick="addDetails()" class="btn btn-info btn-xs">Tambah Detail</a>
                            </div><!--pull-right-->

                            <br/>
                        </div>
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.invoice.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@endsection

@section('after-scripts')
    {{ Html::script('js/backend/access/roles/script.js') }}

    <script>
        var detailCount = 0;

        function addDetails() {
            detailCount += 1;
            var inner = '<div class="col-lg-2"> <input type="date" name="detail_date[]" class="form-control" placeholder="Tanggal"> </div> <div class="col-lg-3"> <input type="text" name="detail_desc[]" class="form-control" placeholder="Deskripsi"> </div> <div class="col-lg-3"> <input type="text" name="detail_header[]" class="form-control" placeholder="Detail Grup"> </div> <div class="col-lg-2"> <input type="number" name="detail_debit[]" class="form-control" placeholder="Debit"> </div> <div class="col-lg-2"> <input type="number" name="detail_credit[]" class="form-control" placeholder="Credit"> </div>';
            var newDetail = document.createElement('div');
            newDetail.id = 'invoice-detail-' + detailCount;
            newDetail.classList = 'form-group invoice-detail';
            newDetail.innerHTML = inner;

            document.getElementById('detail-container').appendChild(newDetail);
        }
    </script>
@endsection
