@extends ('backend.layouts.app')

@section ('title', 'Invoice' . ' | ' . trans('labels.backend.access.roles.create'))

@section('page-header')
    <h1>
        Inventory Management
        <small>Invoice</small>
    </h1>
@endsection

@section('content')
    {{ Form::open(['route' => ['admin.invoice.update', $invoices->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'patch', 'id' => 'edit-invoice']) }}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Invoice</h3>
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Tanggal
                    </label>

                    <div class="col-lg-10">
                        <input type="date" name="date" class="form-control" value="{{date('Y-m-d', strtotime($invoices->date))}}" disabled>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Tipe
                    </label>

                    <div class="col-lg-10">
                        <input type="text" name="type" class="form-control" placeholder="Laporan Pengeluaran" value="{{$invoices->type}}" disabled>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Cabang
                    </label>

                    <div class="col-lg-10">
                        <input type="hidden" name="branch" value="{{$invoices->branches->id}}">
                        <input type="text" name="branch_name" class="form-control" placeholder="Jeruk" value="{{$invoices->branches->branch_name}}" disabled>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Detail
                    </label>

                    <div class="col-lg-10">
                        <div class="well well-lg">
                            @php $i = 0 @endphp
                            <div id="detail-container">
                                @foreach ($invoices->details as $detail)
                                <div id="invoice-detail-{{$i}}" class="form-group invoice-detail">
                                    <input type="hidden" name="id[]" value="{{$detail->id}}">
                                    <div class="col-lg-2">
                                        <input type="date" name="detail_date[]" class="form-control" placeholder="Tanggal" value="{{date('Y-m-d', strtotime($detail->detail_date))}}" disabled>
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" name="detail_desc[]" class="form-control" placeholder="Deskripsi" value="{{$detail->detail_desc}}" disabled>
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" name="detail_header[]" class="form-control" placeholder="Detail Grup" value="{{$detail->detail_header}}" disabled>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="text" name="detail_debit[]" class="form-control price" placeholder="Debit" value="Rp.{{number_format($detail->detail_debit)}}" disabled>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="text" name="detail_credit[]" class="form-control price" placeholder="Credit" value="Rp.{{number_format($detail->detail_credit)}}" disabled>
                                    </div>
                                </div>
                                @php $i = $i + 1 @endphp
                                @endforeach
                            </div>

                            <br/>
                        </div>
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.invoice.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@endsection

@section('after-scripts')
    {{ Html::script('js/backend/access/roles/script.js') }}

    <script>
        var detailCount = {{$i}};

        function addDetails() {
            var inner = '<div class="col-lg-2"> <input type="date" name="detail_date[]" class="form-control" placeholder="Tanggal"> </div> <div class="col-lg-3"> <input type="text" name="detail_desc[]" class="form-control" placeholder="Deskripsi"> </div> <div class="col-lg-3"> <input type="text" name="detail_header[]" class="form-control" placeholder="Detail Grup"> </div> <div class="col-lg-2"> <input type="number" name="detail_debit[]" class="form-control" placeholder="Debit"> </div> <div class="col-lg-2"> <input type="number" name="detail_credit[]" class="form-control" placeholder="Credit"> </div>';
            var newDetail = document.createElement('div');
            newDetail.id = 'invoice-detail-' + detailCount;
            newDetail.classList = 'form-group invoice-detail';
            newDetail.innerHTML = inner;

            document.getElementById('detail-container').appendChild(newDetail);
            detailCount += 1;
        }
    </script>
@endsection
