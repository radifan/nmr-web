@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.inventory.management'))

@section('after-styles')
    {{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
@endsection

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.inventory.management') }}
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.access.inventory.list') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.inventory.includes.inventory-header-buttons')
            </div><!--box-tools pull-right-->
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table id="users-table" class="table table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Jumlah</th>
                            <th>Satuan</th>
                            <th>Nama</th>
                            <th>Nama (Bahasa Inggris)</th>
                            <th>Cabang</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($materials as $material)
                        <tr>
                            <td>{{$material->sku}}</td>
                            <td class="price">{{$material->quantity}}</td>
                            <td>{{$material->quantity_type}}</td>
                            <td>{{$material->material_desc_id}}</td>
                            <td>{{$material->material_desc_en}}</td>
                            <td>{{$material->branches->branch_name}}</td>
                            <td>
                                <a href="{{url('/admin/inventory')}}/{{$material->id}}/get-item" class="btn btn-xs btn-warning"><i class="fa fa-minus-square" data-toggle="tooltip" data-placement="top" title="" data-original-title="Get Item"></i></a>
                                <a href="{{url('/admin/inventory')}}/{{$material->id}}/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"></i></a>
                                <a data-method="delete" data-trans-button-cancel="Cancel" data-trans-button-confirm="Delete" data-trans-title="Are you sure?" class="btn btn-xs btn-danger" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                                    <form action="{{url('/admin/inventory')}}/{{$material->id}}" method="POST" id="delete-item-{{$material->id}}" style="display:none">
                                        <input type="hidden" name="_method" value="delete">
                                        {{ csrf_field() }}
                                    </form>
                                </a>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!--table-responsive-->

            {{$materials->links()}}
        </div><!-- /.box-body -->
    </div><!--box-->

    @permission('view-history')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            {!! history()->renderType('Inventory') !!}
        </div><!-- /.box-body -->
    </div><!--box box-success-->
    @endauth

@endsection

@section('after-scripts')
    {{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}

    <!-- <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("admin.access.user.get") }}',
                    type: 'post',
                    data: {status: 1, trashed: false}
                },
                columns: [
                    {data: 'id', name: '{{config('access.users_table')}}.id'},
                    {data: 'name', name: '{{config('access.users_table')}}.name'},
                    {data: 'email', name: '{{config('access.users_table')}}.email'},
                    {data: 'confirmed', name: '{{config('access.users_table')}}.confirmed'},
                    {data: 'roles', name: '{{config('access.roles_table')}}.name', sortable: false},
                    {data: 'created_at', name: '{{config('access.users_table')}}.created_at'},
                    {data: 'updated_at', name: '{{config('access.users_table')}}.updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500
            });
        });
    </script> -->
@endsection
