@extends ('backend.layouts.app')

@section ('title', 'Edit Item' . ' | ' . trans('labels.backend.access.roles.create'))

@section('page-header')
    <h1>
        Inventory Management
        <small>Get Item</small>
    </h1>
@endsection

@section('content')
    {{ Form::open(['route' => ['admin.inventory.get-item-post', $material->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'get-material']) }}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Get Item</h3>
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Jumlah diambil
                    </label>

                    <div class="col-lg-10">
                        <input type="number" name="quantity" class="form-control price" placeholder="15" value="{{$material->quantity}}">
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Satuan
                    </label>

                    <div class="col-lg-10">
                        <input type="text" name="quantity_type" class="form-control" placeholder="kg" value="{{$material->quantity_type}}" disabled>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Nama
                    </label>

                    <div class="col-lg-10">
                        <input type="text" name="material_desc_id" class="form-control" placeholder="Jeruk" value="{{$material->material_desc_id}}" disabled>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Nama (Bahasa Inggris)
                    </label>

                    <div class="col-lg-10">
                        <input type="text" name="material_desc_en" class="form-control" placeholder="Orange" value="{{$material->material_desc_en}}" disabled>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Cabang
                    </label>

                    <div class="col-lg-10">
                        <input type="hidden" name="branch" value="{{$material->branch}}">
                        <input type="text" name="branch_name" class="form-control" placeholder="Jeruk" value="{{$material->branches->branch_name}}" disabled>
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.inventory.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit('Get Item', ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@endsection

@section('after-scripts')
    {{ Html::script('js/backend/access/roles/script.js') }}
@endsection
