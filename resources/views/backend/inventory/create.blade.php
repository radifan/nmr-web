@extends ('backend.layouts.app')

@section ('title', 'Add Item' . ' | ' . trans('labels.backend.access.roles.create'))

@section('page-header')
    <h1>
        Inventory Management
        <small>Add Item</small>
    </h1>
@endsection

@section('content')
    {{ Form::open(['route' => 'admin.inventory.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-material']) }}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Add Item</h3>
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Jumlah
                    </label>

                    <div class="col-lg-10">
                        <input type="number" name="quantity" class="form-control price" placeholder="15" >
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Jumlah Minimum Stok
                    </label>

                    <div class="col-lg-10">
                        <input type="number" name="min_quantity" class="form-control price" placeholder="5" >
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Satuan
                    </label>

                    <div class="col-lg-10">
                        <select name="quantity_type" class="form-control">
                            <option>Kg</option>
                            <option>Ons</option>
                            <option>Box</option>
                            <option>Ikat</option>
                            <option>Liter</option>
                            <option>Bungkus</option>
                            <option>Pcs</option>
                            <option>Buah</option>
                            <option>Karton</option>
                            <option>Unit</option>
                            <option>Sisir</option>
                            <option>Batang</option>
                        </select>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Nama
                    </label>

                    <div class="col-lg-10">
                        <input type="text" name="material_desc_id" class="form-control" placeholder="Jeruk" >
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Nama (Bahasa Inggris)
                    </label>

                    <div class="col-lg-10">
                        <input type="text" name="material_desc_en" class="form-control" placeholder="Orange" >
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Cabang
                    </label>

                    <div class="col-lg-10">
                        @if ($isAdmin)
                        <select name="branch" class="form-control">
                            @foreach ($branch as $b)
                            <option value="{{$b->id}}">{{$b->branch_name}}</option>
                            @endforeach
                        </select>
                        @else
                        <input type="hidden" name="branch" value="{{$branch->id}}">
                        <input type="text" name="branch_name" class="form-control" placeholder="Jeruk" value="{{$branch->branch_name}}" disabled>
                        @endif
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.inventory.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@endsection

@section('after-scripts')
    {{ Html::script('js/backend/access/roles/script.js') }}
@endsection
