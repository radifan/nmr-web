@extends ('backend.layouts.app')

@section ('title', 'Edit Item' . ' | ' . trans('labels.backend.access.roles.create'))

@section('page-header')
    <h1>
        Inventory Management
        <small>Edit Item</small>
    </h1>
@endsection

@section('content')
    {{ Form::open(['route' => ['admin.inventory.update', $material->id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'patch', 'id' => 'edit-material']) }}

        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Item</h3>
            </div><!-- /.box-header -->

            <div class="box-body">
                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Jumlah
                    </label>

                    <div class="col-lg-10">
                        <input type="number" name="quantity" class="form-control price" placeholder="15" value="{{$material->quantity}}">
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Satuan
                    </label>

                    <div class="col-lg-10">
                        <select name="quantity_type" class="form-control">
                            <option {{ $material->quantity_type == "Kg" ? "selected" : "" }}>Kg</option>
                            <option {{ $material->quantity_type == "Ons" ? "selected" : "" }}>Ons</option>
                            <option {{ $material->quantity_type == "Box" ? "selected" : "" }}>Box</option>
                            <option {{ $material->quantity_type == "Ikat" ? "selected" : "" }}>Ikat</option>
                            <option {{ $material->quantity_type == "Liter" ? "selected" : "" }}>Liter</option>
                            <option {{ $material->quantity_type == "Bungkus" ? "selected" : "" }}>Bungkus</option>
                            <option {{ $material->quantity_type == "Pcs" ? "selected" : "" }}>Pcs</option>
                            <option {{ $material->quantity_type == "Buah" ? "selected" : "" }}>Buah</option>
                            <option {{ $material->quantity_type == "Karton" ? "selected" : "" }}>Karton</option>
                            <option {{ $material->quantity_type == "Unit" ? "selected" : "" }}>Unit</option>
                            <option {{ $material->quantity_type == "Sisir" ? "selected" : "" }}>Sisir</option>
                            <option {{ $material->quantity_type == "Batang" ? "selected" : "" }}>Batang</option>
                        </select>
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Nama
                    </label>

                    <div class="col-lg-10">
                        <input type="text" name="material_desc_id" class="form-control" placeholder="Jeruk" value="{{$material->material_desc_id}}">
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Nama (Bahasa Inggris)
                    </label>

                    <div class="col-lg-10">
                        <input type="text" name="material_desc_en" class="form-control" placeholder="Orange" value="{{$material->material_desc_en}}">
                    </div><!--col-lg-10-->
                </div><!--form control-->

                <div class="form-group">
                    <label class="col-lg-2 control-label">
                        Cabang
                    </label>

                    <div class="col-lg-10">
                        @if (count($branch) > 0)
                        <select name="branch" class="form-control">
                            @foreach ($branch as $b)
                            <option value="{{$b->id}}" {{$b->id == $material->branch ? "selected" : ""}}>{{$b->branch_name}}</option>
                            @endforeach
                        </select>
                        @else
                        <input type="hidden" name="branch" value="{{$material->branch}}">
                        <input type="text" name="branch_name" class="form-control" placeholder="Jeruk" value="{{$material->branches->branch_name}}" disabled>
                        @endif
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div><!-- /.box-body -->
        </div><!--box-->

        <div class="box box-success">
            <div class="box-body">
                <div class="pull-left">
                    {{ link_to_route('admin.inventory.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-xs']) }}
                </div><!--pull-left-->

                <div class="pull-right">
                    {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-success btn-xs']) }}
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->

    {{ Form::close() }}
@endsection

@section('after-scripts')
    {{ Html::script('js/backend/access/roles/script.js') }}
@endsection
