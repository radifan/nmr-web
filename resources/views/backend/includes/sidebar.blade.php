<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ access()->user()->picture }}" class="img-circle" alt="User Image" />
            </div><!--pull-left-->
            <div class="pull-left info" style="margin-top: 10px">
                <p>{{ access()->user()->name }}</p>
            </div><!--pull-left-->
        </div><!--user-panel-->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">

            <li class="{{ active_class(Active::checkUriPattern('admin/dashboard')) }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{ trans('menus.backend.sidebar.dashboard') }}</span>
                </a>
            </li>

            <!-- Inventory Menu -->

            <li class="{{ active_class(Active::checkUriPattern('admin/inventory*')) }}">
                <a href="{{ route('admin.inventory.index') }}">
                    <i class="fa fa-archive"></i>
                    <span>{{ trans('menus.backend.access.inventory.title') }}</span>
                </a>
            </li>

            <!-- Purchase Order Menu -->

            <li class="{{ active_class(Active::checkUriPattern('admin/purchase-order*')) }} treeview">
                <a href="#">
                    <i class="fa fa-list-alt"></i>
                    <span>{{ trans('menus.backend.access.purchase-order.title') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/po'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/purchase-order*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/po')) }}">
                        <a href="{{ route('admin.po.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.access.purchase-order.order-list') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/po-report')) }}">
                        <a href="{{ route('admin.po-report.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.access.purchase-order.order-report') }}</span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- Invoice Menu -->

            <li class="{{ active_class(Active::checkUriPattern('admin/invoice')) }}">
                <a href="{{ route('admin.invoice.index') }}">
                    <i class="fa fa-sticky-note"></i>
                    <span>{{ trans('menus.backend.access.invoice.title') }}</span>
                </a>
            </li>

            @permission('approve-purchase-orders')
            <li class="{{ active_class(Active::checkUriPattern('admin/approval')) }}">
                <a href="{{ route('admin.approval.index') }}">
                    <i class="fa fa-check"></i>
                    <span>Approval</span>
                </a>
            </li>
            @endauth

            @permissions(['manage-users', 'manage-roles'])
            <li class="{{ active_class(Active::checkUriPattern('admin/access/*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Users</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/access/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/access/*'), 'display: block;') }}">
                    @permission('manage-users')
                    <li class="{{ active_class(Active::checkUriPattern('admin/access/user*')) }}">
                        <a href="{{ route('admin.access.user.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.users.management') }}</span>
                        </a>
                    </li>
                    @endauth

                    @permission('manage-roles')
                    <li class="{{ active_class(Active::checkUriPattern('admin/access/role*')) }}">
                        <a href="{{ route('admin.access.role.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.roles.management') }}</span>
                        </a>
                    </li>
                    @endauth
                </ul>
            </li>
            @endauth

            <!-- <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer*')) }} treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>{{ trans('menus.backend.log-viewer.main') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer')) }}">
                        <a href="{{ route('log-viewer::dashboard') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.dashboard') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer/logs')) }}">
                        <a href="{{ route('log-viewer::logs.list') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.logs') }}</span>
                        </a>
                    </li>
                </ul>
            </li> -->
        </ul><!-- /.sidebar-menu -->
    </section><!-- /.sidebar -->
</aside>