<?php

/*
 * Inventory Management
 */
Route::group([
    'middleware' => 'access.routeNeedsPermission:manage-purchase-orders',
], function () {
    Route::group(['namespace' => 'PurchaseOrderReport'], function () {
        Route::resource('po-report', 'PurchaseOrderReportController');
    });
});
