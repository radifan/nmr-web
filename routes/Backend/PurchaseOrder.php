<?php

/*
 * Inventory Management
 */
Route::group([
    'middleware' => 'access.routeNeedsPermission:manage-purchase-orders',
], function () {
    Route::group(['namespace' => 'PurchaseOrder'], function () {
        Route::resource('po', 'PurchaseOrderController');
        Route::get('po-print/{id}', 'PurchaseOrderController@printPO');
    });
});
