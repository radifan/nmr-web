<?php

/*
 * Inventory Management
 */
Route::group([
    'middleware' => 'access.routeNeedsPermission:approve-purchase-orders',
], function () {
    Route::group(['namespace' => 'Approval'], function () {
        Route::resource('approval', 'ApprovalController');
        Route::get('approve/{id}', 'ApprovalController@approve');
    });
});
