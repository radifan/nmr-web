<?php

/*
 * Inventory Management
 */
Route::group([
    'middleware' => 'access.routeNeedsPermission:manage-invoice',
], function () {
    Route::group(['namespace' => 'Invoice'], function () {
        Route::resource('invoice', 'InvoiceController');
    });
});
