<?php

/*
 * Inventory Management
 */
Route::group([
    'middleware' => 'access.routeNeedsPermission:manage-inventory',
], function () {
    Route::group(['namespace' => 'Inventory'], function () {
        Route::resource('inventory', 'InventoryController');
        Route::get('inventory/{inventory}/get-item', 'InventoryController@getItem')->name('inventory.get-item');
        Route::post('inventory/{inventory}/get-item', 'InventoryController@getItemPost')->name('inventory.get-item-post');
    });
});
